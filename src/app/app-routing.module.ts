import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./tab1/modal/modal.module').then( m => m.ModalPageModule)
  },
  {
    path: 'ig-player',
    loadChildren: () => import('./ig-player/ig-player.module').then( m => m.IgPlayerPageModule)
  },
  {
    path: 'end-screen',
    loadChildren: () => import('./end-screen/end-screen.module').then( m => m.EndScreenPageModule)
  },
  {
    path: 'lst-participant',
    loadChildren: () => import('./lst-participant/lst-participant.module').then( m => m.LstParticipantPageModule)
  },  {
    path: 'ig-animateur',
    loadChildren: () => import('./ig-animateur/ig-animateur.module').then( m => m.IgAnimateurPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
