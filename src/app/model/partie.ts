import { User } from './user';

export interface Partie {
    id: number;
    placeId: number;
    nom: string;
    joueurs: Array<User>;
    
  }