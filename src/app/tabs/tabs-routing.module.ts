import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },
      {
        path: 'ig-player',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../ig-player/ig-player.module').then(m => m.IgPlayerPageModule)
          }
        ]
      },
      {
        path: 'ig-animateur',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../ig-animateur/ig-animateur.module').then(m => m.IgAnimateurPageModule)
          }
        ]
      },
      {
        path: 'end-screen',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../end-screen/end-screen.module').then(m => m.EndScreenPageModule)
          }
        ]
      },
      {
        path: 'lst-participant',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../lst-participant/lst-participant.module').then(m => m.LstParticipantPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
