import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndScreenPageRoutingModule } from './end-screen-routing.module';

import { EndScreenPage } from './end-screen.page';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule,
    EndScreenPageRoutingModule
  ],
  declarations: [EndScreenPage]
})
export class EndScreenPageModule {}
