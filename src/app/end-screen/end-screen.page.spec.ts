import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EndScreenPage } from './end-screen.page';

describe('EndScreenPage', () => {
  let component: EndScreenPage;
  let fixture: ComponentFixture<EndScreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndScreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EndScreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
