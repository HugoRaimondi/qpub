import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EndScreenPage } from './end-screen.page';

const routes: Routes = [
  {
    path: '',
    component: EndScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndScreenPageRoutingModule {}
