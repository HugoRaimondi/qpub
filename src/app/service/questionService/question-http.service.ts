import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionHttpService {


  constructor(private http: HttpClient) { }

  findAll(): Observable<any> {
    return this.http.get("http://51.178.47.135:9050/question/findAll");
  }
}
