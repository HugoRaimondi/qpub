import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IgAnimateurPageRoutingModule } from './ig-animateur-routing.module';

import { IgAnimateurPage } from './ig-animateur.page';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule,
    IgAnimateurPageRoutingModule
  ],
  declarations: [IgAnimateurPage]
})
export class IgAnimateurPageModule {}
