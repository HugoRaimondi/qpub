import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IgAnimateurPage } from './ig-animateur.page';

const routes: Routes = [
  {
    path: '',
    component: IgAnimateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IgAnimateurPageRoutingModule {}
