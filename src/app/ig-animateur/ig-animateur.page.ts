import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { User } from '../model/user';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Partie } from '../model/partie';
import { UserHttpService } from '../service/userService/user-http.service';
import { Question } from '../model/question';
import { QuestionHttpService } from '../service/questionService/question-http.service';

@Component({
  selector: 'app-ig-animateur',
  templateUrl: './ig-animateur.page.html',
  styleUrls: ['./ig-animateur.page.scss'],
})
export class IgAnimateurPage implements OnInit {

  displayedColumns: Array<string>;
  users: Array<User>;
  partie: Partie
  questions: Array<Question>
  question: Question = {question:'Champ question', reponse:'Champ réponse' };

  dataSource: MatTableDataSource<User>;
  selection = new SelectionModel<User>(true, []);
  constructor(private userhttp : UserHttpService, private questionhttp : QuestionHttpService) { }
  
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {

    this.displayedColumns = ["name", "point"];
    this.findAllQuestion();
    this.findAllUser();
    
    
  }

  findAllUser() {
    this.userhttp.findAll().subscribe((partie) => {
      this.partie = partie;
      this.users = this.partie.joueurs;
      this.initDataSource();
    });
  }

  findAllQuestion() {
    this.questionhttp.findAll().subscribe((question) => {
      this.questions = question;
      this.question = this.questions[Math.floor(Math.random() * this.questions.length)];
      console.log(this.question);


    });
  }

  initDataSource() {
    this.dataSource = new MatTableDataSource<User>(this.users);
    this.dataSource.sort = this.sort;
  }

  reload(){
    this.ngOnInit();
  }
}
