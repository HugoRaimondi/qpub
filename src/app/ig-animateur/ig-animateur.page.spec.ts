import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IgAnimateurPage } from './ig-animateur.page';

describe('IgAnimateurPage', () => {
  let component: IgAnimateurPage;
  let fixture: ComponentFixture<IgAnimateurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IgAnimateurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IgAnimateurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
