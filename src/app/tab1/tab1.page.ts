import { Component, Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPage } from './modal/modal.page';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
@Injectable()

/**
 * @class Tab1Page
 * First page of the application
 */
export class Tab1Page {

  /**
   * Informations of the form
   */
  userForm: FormGroup;

  constructor(public modalController: ModalController, private http: HttpClient, public router: Router) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      username: new FormControl("", []),
      town: new FormControl("", []),
      adress: new FormControl("", [])
    });
  }

  /**
   * Try it, you'll see
   * @returns {void}
   */
  easterEgg(): void {
    window.open("https://youtu.be/cLmCJKT5ssw?t=65", "_blank");
  }

  /** 
  * Open component modalComponent. 
  * 
  * @return {Promise<void>} 
  */
  async presentAlert(): Promise<void> {
    const modal = await this.modalController.create({
      component: ModalPage
    });
    return await modal.present();
  }

  /**
   * Verify if input are not empty
   * 
   * @return {boolean}
   */
  isComplete(): boolean {
    if (this.userForm.get("username").value != "" && this.userForm.get("town").value != "" && this.userForm.get("adress").value != "") {
      return false;
    }
    return true;

  }

  /**
   * Get adress, town and open component tab2
   * 
   * @returns {void}
   */
  getall(): void {
    let adresse = this.userForm.get("adress").value;
    let ville = this.userForm.get("town").value;
    this.router.navigate(['/tabs/tab2'], { queryParams: { town: ville, adress: adresse } });
  }
}