import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LstParticipantPage } from './lst-participant.page';

const routes: Routes = [
  {
    path: '',
    component: LstParticipantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LstParticipantPageRoutingModule {}
