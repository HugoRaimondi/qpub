import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { User } from '../model/user';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Partie } from '../model/partie';
import { UserHttpService } from '../service/userService/user-http.service';
@Component({
  selector: 'app-lst-participant',
  templateUrl: './lst-participant.page.html',
  styleUrls: ['./lst-participant.page.scss'],
})
export class LstParticipantPage implements OnInit {

  displayedColumns: Array<string>;
  users: Array<User>;
  partie: Partie

  dataSource: MatTableDataSource<User>;
  selection = new SelectionModel<User>(true, []);
  constructor(private userhttp : UserHttpService) { }
  
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {

    this.displayedColumns = ["name", "point"];

    this.findAllUser();
  }

  findAllUser() {
    this.userhttp.findAll().subscribe((partie) => {
      this.partie = partie;
      this.users = this.partie.joueurs;
      this.initDataSource();
    });
  }

  initDataSource() {
    this.dataSource = new MatTableDataSource<User>(this.users);
    this.dataSource.sort = this.sort;
  }

}
