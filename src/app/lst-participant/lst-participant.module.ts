import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';

import { IonicModule } from '@ionic/angular';

import { LstParticipantPageRoutingModule } from './lst-participant-routing.module';

import { LstParticipantPage } from './lst-participant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule,
    LstParticipantPageRoutingModule
  ],
  declarations: [LstParticipantPage]
})
export class LstParticipantPageModule {}
