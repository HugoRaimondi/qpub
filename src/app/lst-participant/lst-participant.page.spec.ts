import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LstParticipantPage } from './lst-participant.page';

describe('LstParticipantPage', () => {
  let component: LstParticipantPage;
  let fixture: ComponentFixture<LstParticipantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LstParticipantPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LstParticipantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
