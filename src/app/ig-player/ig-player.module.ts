import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IgPlayerPageRoutingModule } from './ig-player-routing.module';

import { IgPlayerPage } from './ig-player.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IgPlayerPageRoutingModule
  ],
  declarations: [IgPlayerPage]
})
export class IgPlayerPageModule {}
