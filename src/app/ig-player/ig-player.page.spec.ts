import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IgPlayerPage } from './ig-player.page';

describe('IgPlayerPage', () => {
  let component: IgPlayerPage;
  let fixture: ComponentFixture<IgPlayerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IgPlayerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IgPlayerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
