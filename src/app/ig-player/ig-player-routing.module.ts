import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IgPlayerPage } from './ig-player.page';

const routes: Routes = [
  {
    path: '',
    component: IgPlayerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IgPlayerPageRoutingModule {}
