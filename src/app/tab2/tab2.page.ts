import { Component, Input } from '@angular/core';
import { Map, latLng, tileLayer, Layer, marker } from 'leaflet';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

/**
 * @class Tab2Page
 */
export class Tab2Page {
  /** Map object */
  map: Map;
  /** GPS coordinates */
  coord: String;
  /** Adress of the player */
  adresse: String;
  /** City of the player */
  ville: String;
  /** Adress and city */
  adresseComplete: String;

  constructor(public route: ActivatedRoute,
    public router: Router, private http: HttpClient) { }

  ionViewDidEnter() {
    this.ville = this.route.snapshot.queryParamMap.get("town");
    this.adresse = this.route.snapshot.queryParamMap.get("adress");
    this.adresseComplete = this.adresse + ", " + this.strUcFirst(this.ville);
    this.http.get("https://nominatim.openstreetmap.org/search?format=json&street=" + this.adresse + "&city=" + this.ville + "&country=france")
      .subscribe(pos => {
        this.coord = pos[0].lat + "," + pos[0].lon;
        this.leafletMap();
      });
  }

  /**
   * Create the map and get the coordinates to locate the given adress
   * @return {void}
   */
  leafletMap(): void {
    var lat = this.coord.split(",")[0];
    var long = this.coord.split(",")[1];
    // Get lat and long and focus the map on this pos
    this.map = new Map('mapId').setView([lat, long], 15);
    tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'edupala.com © ionic LeafLet',
    }).addTo(this.map);

    // Set a marker on the map
    marker([lat, long]).addTo(this.map)
      .bindPopup('Vous êtes ici')
      .openPopup();
  }

  /** Remove map when we have multiple map object */
  ionViewWillLeave() {
    this.map.remove();
  }

  /**
   * Get the first letter and change it to uppercase
   * @param {string} a String to change
   * @return {string}
   */
  strUcFirst(a: String): string { return (a + '').charAt(0).toUpperCase() + a.substr(1); }

  changeToSelectRole(){
    this.router.navigate(['/tabs/tab3'], { queryParams: { coord: this.coord } });
  }
}
