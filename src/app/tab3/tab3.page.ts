import { Component, Input } from '@angular/core';
import { Map, latLng, tileLayer, Layer, marker } from 'leaflet';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

    /** Map object */
    map: Map;
    /** GPS coordinates */
    coord: String;
    constructor(public route: ActivatedRoute,
      public router: Router, private http: HttpClient) { }
  
    ionViewDidEnter() {
      this.coord = this.route.snapshot.queryParamMap.get("coord");
      console.log(this.coord)
      this.leafletMap();
    }
  
    /**
     * Create the map and get the coordinates to locate the given adress
     * @return {void}
     */
    leafletMap(): void {
      var lat = this.coord.split(",")[0];
      var long = this.coord.split(",")[1];
      // Get lat and long and focus the map on this pos
      this.map = new Map('mapId-players').setView([lat, long], 15);
      tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'edupala.com © ionic LeafLet',
      }).addTo(this.map);
  
      // Set a marker on the map
      marker([lat, long]).addTo(this.map)
        .openPopup();
    }
  
    /** Remove map when we have multiple map object */
    ionViewWillLeave() {
      this.map.remove();
    }
  }
